# Installation

`pip install -r requirements.txt`

# Usage

`python smudges.py --image=X --algorithm=Y`

Documentation can be produced by running:
`python smudges.py -h` 

Running the software will produce a series of intermediate renderings before saving the output to filename_output.tif
