FROM registry.gitlab.com/b0ggyb33/circle-detector/cpp-environment
WORKDIR /app
COPY * ./
RUN mkdir build
WORKDIR /app/build
RUN cmake ..
RUN make
ENTRYPOINT ["./runTests"]