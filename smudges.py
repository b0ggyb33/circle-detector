import cv2
import numpy as np


def load_image(filename):
    assert filename.exists()
    return cv2.imread(str(filename))


def normalise_image(im):
    g_im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY).astype('float64')
    g_im -= g_im.min()
    g_im *= (255 / (g_im.max()))
    return g_im.astype('uint8')


def laplacian(im):
    return cv2.Laplacian(im, cv2.CV_64F)


def gaussian_blur(im):
    return cv2.blur(im.astype('uint8'), (7, 7))


def hough(im):
    method = cv2.HOUGH_GRADIENT
    dp = 1
    minDist = 3

    return cv2.HoughCircles(im.astype('uint8'), method, dp, minDist, param1=50, param2=10, minRadius=3, maxRadius=6)


def threshold(im):
    return cv2.adaptiveThreshold(im, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 5, 0)


def binary_open(im):
    kernel = np.ones((3, 3), np.uint8)
    opened_im = cv2.morphologyEx(im, cv2.MORPH_OPEN, kernel,iterations=1)
    return opened_im


def find_smudges_one(im):
    g_im = normalise_image(im)
    render("normalised", g_im)
    blur = gaussian_blur(g_im)
    render("blurred", blur)
    filtered = laplacian(threshold(blur))
    render("filtered", filtered)
    circles = hough(filtered)[0]
    return circles


def find_smudges_two(im):
    g_im = normalise_image(im)
    blur = gaussian_blur(g_im)
    render("blurred", blur)
    t_blur = threshold(blur)
    render("thresholded", t_blur)

    opened = binary_open(t_blur)
    t_blur_2 = threshold(opened)
    render("thresholded2", t_blur_2)

    edges = laplacian(t_blur_2)
    render("edge detected", edges)
    circles = hough(edges)[0]
    return circles


def render(name, im):
    cv2.imshow(name, im)
    cv2.waitKey(0)


if __name__ == "__main__":
    import argparse
    import pathlib
    p = argparse.ArgumentParser(description="Extracts luminescent points in the image and saves to file.")
    p.add_argument("--image", default=r"G-ex_P100_50deg_exp100_2017-05-18_15h25m02s936ms-1.tif", help="Path to the tif image to process.")
    p.add_argument("--algorithm", type=int, default=0, help="Choose between 0 and 1 to pick the algorithm used.")
    args = p.parse_args()
    path = pathlib.Path(args.image)
    im = load_image(path)
    circles = [find_smudges_one, find_smudges_two][args.algorithm](im)

    out = im.copy()
    for circle in circles:
        x, y, r = circle
        cv2.rectangle(out, (x - r, y - r), (x + r, y + r), color=(255, 0, 0))

    render("smudges approach {} output".format(args.algorithm+1), out)

    output_name = path.stem + '_output_{}_'.format(args.algorithm+1) + path.suffix
    cv2.imwrite(output_name, out)



